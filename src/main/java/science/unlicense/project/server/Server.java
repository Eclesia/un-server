
package science.unlicense.project.server;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.format.json.HJSONReader;
import science.unlicense.format.json.JSONUtilities;

/**
 *
 * @author Johann Sorel
 */
public class Server {

    public static void main(String[] args) throws IOException {

        final String path;
        if (args.length==0) {
            path = "./server.hjson";
        } else {
            path = args[0];
        }

        final Path configPath = Paths.resolve(new Chars(path));
        final HJSONReader configReader = new HJSONReader();
        configReader.setInput(configPath);
        final Document config = JSONUtilities.readAsDocument(configReader, null);

        new HTTPPack(config).start();

    }

}
