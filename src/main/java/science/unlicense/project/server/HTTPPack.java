
package science.unlicense.project.server;

import science.unlicense.common.api.character.Chars;
import science.unlicense.common.api.collection.Dictionary;
import science.unlicense.common.api.collection.HashDictionary;
import science.unlicense.common.api.logging.Logger;
import science.unlicense.common.api.model.doc.Document;
import science.unlicense.concurrent.api.Formats;
import science.unlicense.concurrent.api.Paths;
import science.unlicense.encoding.api.io.ByteInputStream;
import science.unlicense.encoding.api.io.ByteOutputStream;
import science.unlicense.encoding.api.io.IOException;
import science.unlicense.encoding.api.io.IOUtilities;
import science.unlicense.encoding.api.logging.FileLogger;
import science.unlicense.encoding.api.path.Path;
import science.unlicense.encoding.api.store.Format;
import science.unlicense.protocol.http.AbstractSimpleClient;
import science.unlicense.protocol.http.EmptyRequestException;
import science.unlicense.protocol.http.HTTPConstants;
import science.unlicense.protocol.http.HTTPMessageBody;
import science.unlicense.protocol.http.HTTPRequest;
import science.unlicense.protocol.http.HTTPResponse;
import science.unlicense.protocol.http.HTTPServer;
import science.unlicense.protocol.http.HTTPVersion;
import science.unlicense.system.ClientSocket;

/**
 *
 * @author Johann Sorel
 */
public class HTTPPack extends HTTPServer {

    private static final Chars ATT_ROOT = new Chars("root");
    private static final Chars ATT_PORT = new Chars("port");
    private static final Chars ATT_REDIRECTIONS = new Chars("redirections");
    private static final Chars ATT_REQUEST = new Chars("request");
    private static final Chars ATT_TARGET = new Chars("target");
    private static final Chars ATT_LOGPATH = new Chars("logPath");

    private final Path root;
    private final Dictionary redirections = new HashDictionary();
    private final Logger LOGGER;

    public HTTPPack(Document config) throws IOException {
        super((Integer) config.getPropertyValue(ATT_PORT));
        root = Paths.resolve((Chars) config.getPropertyValue(ATT_ROOT));
        Path logPath = Paths.resolve((Chars) config.getPropertyValue(ATT_LOGPATH));

        final Object[] redirections = (Object[]) config.getPropertyValue(ATT_REDIRECTIONS);
        if (redirections != null) {
            for (int i=0;i<redirections.length;i++) {
                final Document redir = (Document) redirections[i];
                final Object request = redir.getPropertyValue(ATT_REQUEST);
                final Object target = redir.getPropertyValue(ATT_TARGET);
                this.redirections.add(request, target);
            }
        }
        LOGGER = new FileLogger(logPath);
    }

    @Override
    public void create(ClientSocket clientsocket) {
        new PathClientHandler(clientsocket);
    }

    private class PathClientHandler extends AbstractSimpleClient {

        public PathClientHandler(ClientSocket socket) {
            super(socket);
        }

        @Override
        protected HTTPResponse treat(HTTPRequest request) {
            LOGGER.info(request.getUrl());

            final HTTPResponse response = new HTTPResponse();
            response.setVersion(HTTPVersion.V1_1);
            response.getHeaders().add(HTTPConstants.Response.HEADER_SERVER, new Chars("Unlicense-server"));

            Chars url = request.getUrl();

            //apply redirections
            Object redir = redirections.getValue(url);
            if (redir != null) url = (Chars) redir;

            if (url.startsWith('/')) url = url.truncate(1, -1);

            if (url.endsWith('/')) url = url.concat(new Chars("index.html"));

            final Path p = root.resolve(url);

            try {
                if (p != null && p.exists() && !p.isContainer()) {
                    response.setCode(HTTPConstants.STATUS_200_OK);

                    //security check, ensure the path is not a parent path
                    boolean isParent = false;
                    Path prt = p;
                    while (prt != null) {
                        if (prt.equals(root)) {
                            isParent = true;
                            break;
                        }
                        prt = prt.getParent();
                    }

                    if (!isParent) {
                        response.setCode(HTTPConstants.STATUS_404_NOT_FOUND);
                        return response;
                    }

                    Chars contentType = null;

                    final Format format = Formats.findFormat(p);
                    if (format != null && !format.getMimeTypes().isEmpty()) {
                        contentType = (Chars) format.getMimeTypes().get(0);
                    }

                    if (contentType == null) {
                        contentType = new Chars("application/octet-stream");
                    }

                    response.setMessageBody(new HTTPMessageBody() {
                        @Override
                        public void write(ByteOutputStream out) throws IOException {
                            ByteInputStream in = p.createInputStream();
                            IOUtilities.copy(in, out);
                            in.dispose();
                        }

                        @Override
                        public void read(ByteInputStream in) throws IOException {
                            throw new UnsupportedOperationException("Not supported yet.");
                        }
                    });

                    response.getHeaders().add(HTTPConstants.Response.HEADER_CONTENT_TYPE, contentType);

                } else {
                    response.setCode(HTTPConstants.STATUS_404_NOT_FOUND);
                }
            } catch (IOException ex) {
                LOGGER.warning(ex);
                response.setCode(HTTPConstants.STATUS_500_INTERNAL_ERROR);
            }

            return response;
        }

        @Override
        protected void execeptionOccured(Exception ex) {
            if (ex instanceof EmptyRequestException) {
                LOGGER.info(ex);
            } else {
                LOGGER.critical(ex);
            }
        }
    }

}
